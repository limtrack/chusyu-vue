// Mixins
import interfaceMixin from './interfaceMixin';

const get = require('lodash.get');

export default {
  mixins: [interfaceMixin],
  data() {
    return {
      errorDefaultForm: 'Something was wrong!!!',
      processingForm: false,
      errorInForm: false,
    };
  },
  computed: {
    /**
     * Check if all formulary fields are filled correctly
     * @return {boolean}
     */
    isFormCompleted() {
      if (this.$v.$params) {
        return !Object.keys(this.$v.$params)
          .some((key) => this.$v[key].$dirty && this.$v[key].$error);
      }
      return true;
    },
  },
  methods: {
    /**
     * Default method to submit form.
     * It is possible that the child view has a owner method
     */
    onSubmit() {
      if (typeof this.beforeSubmit === 'function') {
        this.beforeSubmit()
          .then(() => {
            this.actionSubmit()
              .then(() => {
                if (typeof this.afterSubmit === 'function') {
                  this.afterSubmit();
                }
              });
          })
          .catch(() => {
            this.showErrorSubmit();
          });
      } else {
        this.actionSubmit()
          .then(() => {
            if (typeof this.afterSubmit === 'function') {
              this.afterSubmit();
            }
          });
      }
    },
    /**
     * Submit action
     * @return {promise}
     */
    actionSubmit() {
      return new Promise((resolve, reject) => {
        this.$v.$touch();
        if (!this.$v.$invalid) {
          this.processingForm = true;
          resolve();
        } else {
          this.showErrorSubmit();
          reject();
        }
      });
    },
    /**
     * Default action with the server response
     * @param {object} response - response from server
     * @return {Boolean} success response or error response
     */
    defaultResponseServer(response) {
      const currentResponseSuccess = get(response, 'data.success', false);
      const currentResponseMsg = get(response, 'data.msg', false);

      if (currentResponseSuccess === false) {
        this.showAlert({
          content: currentResponseMsg || this.errorDefaultForm,
          variant: 'warning',
        });
        return false;
      }
      return true;
    },
    /**
     * Show error submit action
     */
    showErrorSubmit() {
      this.showAlert({
        content: this.errorForm || this.errorDefaultForm,
        variant: 'warning',
      });
    },
    /**
     * Get status input to attribute "state"
     * Only the states "null" (pristine) and "false" (error)
     * @param {string} field - name field in model
     * @return {null | false} Pristine or Error
     */
    getStatusInput(field) {
      const dirty = get(this, `$v.${field}.$dirty`, false);
      const error = get(this, `$v.${field}.$error`, false);

      if (dirty && error) {
        return false;
      }
      return null;
    },
    /**
     * Touch field input
     * @param {string} field - name field in model
     */
    touchField(field) {
      const currentField = get(this, `$v.${field}`, false);
      if (currentField) {
        currentField.$touch();
      }
    },
  },
};
