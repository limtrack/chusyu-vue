// Actions (Vuex)
import actionsName from '@/store/actionsName';
// Componets Actions (Vuex)
import alertActions from '@/components/AppAlert/store/actionsName';
import modalActions from '@/components/AppModal/store/actionsName';
import loadingLayerActions from '@/components/AppLoadingLayer/store/actionsName';
// Constants
import constants from '@/constants';

const { APP_MAIN_MODAL_ID, VUEX_MODULES, LOGIN_PATH } = constants;

const { UPDATE_RESET_MODEL } = actionsName;
const { UPDATE_APP_ALERT_MODEL } = alertActions;
const { UPDATE_APP_MODAL_MODEL } = modalActions;
const { UPDATE_APP_LOADING_LAYER_MODEL } = loadingLayerActions;

export default {
  data() {
    return {
      msgClosedSession: 'Se ha cerrado la sesión del usuario',
    };
  },
  methods: {
    // SESSION FUNCTIONS

    /**
     * Close session deleting all state content
     */
    closeSession() {
      this.$store.dispatch(UPDATE_RESET_MODEL);
      this.$router.replace(LOGIN_PATH);
      this.showAlert({
        content: this.msgClosedSession,
        variant: 'primary',
      });
    },

    // LAYOUT FUNCTIONS

    /**
     * Modify modal content
     * @param {Object} params - modal options content
     * @param {Object} options - modal options
     */
    modifyModal(params, options) {
      this.$store.dispatch(`${VUEX_MODULES.appModal}/${UPDATE_APP_MODAL_MODEL}`, params);
      if (options.show) {
        this.showModal(options.id);
      }
    },

    /**
     * Show modal
     * @param {String} modalId - modal id
     */
    showModal(modalId = APP_MAIN_MODAL_ID) {
      this.$root.$emit('bv::show::modal', modalId);
    },

    /**
     * Hide modal
     * @param {String} modalId - modal id
     */
    hideModal(modalId = APP_MAIN_MODAL_ID) {
      this.$root.$emit('bv::hide::modal', modalId);
    },

    /**
     * Modify alert content
     * @param {Object} params - alert options
     */
    showAlert(params) {
      this.$store.dispatch(`${VUEX_MODULES.appAlert}/${UPDATE_APP_ALERT_MODEL}`, params);
    },

    /**
     * Modify loading layer content
     * @param {Object} params - alert options
     */
    showLoadingLayer(params) {
      this.$store.dispatch(`${VUEX_MODULES.appLoadingLayer}/${UPDATE_APP_LOADING_LAYER_MODEL}`, params);
    },
  },
};
