import Vue from 'vue';
import Router from 'vue-router';

// Constants
import constants from '@/constants';
// Routes
import routesName from './routesName';
// Others
import actions from '@/store/actions';
import store from '@/store/index';

const routes = routesName;
const { LOGIN_PATH } = constants;
const { UPDATE_REFERER_URL } = actions;

Vue.use(Router);

const router = new Router({
  routes,
  // eslint-disable-next-line no-unused-vars
  scrollBehavior(to, from, savedPosition) {
    // Always on top when change
    // the path into the app
    return { x: 0, y: 0 };
  },
});

router.beforeEach((to, from, next) => {
  if ((to.meta && to.meta.isPublic) || store.getters.isUserLogged) {
    next();
  } else {
    store.dispatch(UPDATE_REFERER_URL, to);
    next({ path: LOGIN_PATH });
  }
});

export default router;
