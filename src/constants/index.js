// Import components config files
import AppAlertConfig from '@/components/AppAlert/config';
import AppLoadingLayerConfig from '@/components/AppLoadingLayer/config';
import AppModalConfig from '@/components/AppModal/config';

// config files in component
const componentsConfig = {
  appAlert: AppAlertConfig,
  appLoadingLayer: AppLoadingLayerConfig,
  appModal: AppModalConfig,
};

// Vuex modules
const vuexModules = Object.entries(componentsConfig)
  .reduce((sumConfig, currentConfig) => {
    if (currentConfig[1] && currentConfig[1].vuex) {
      // eslint-disable-next-line no-param-reassign
      sumConfig[currentConfig[0]] = currentConfig[1].name;
    }
    return sumConfig;
  }, {});

export default {
  /**
   * LocalStorage
   * */
  LOCALSTORAGE_VUEX_KEY: 'chusyu', // How save the Vuex object
  LOCALSTORAGE_TOKEN_PATH: 'user.token', // Path where is save token

  /**
   * MODULES
   * */
  // Vuex modules
  VUEX_MODULES: vuexModules,

  /**
   * SETUP APP
   * */
  // Flow app
  INIT_APP: '/ideas', // Initial path (view)
  LOGIN_PATH: '/user-login', // Login path
  // DOM elements
  APP_ID: 'app',
  APP_MAIN_ALERT_ID: 'app-main-alert',
  APP_MAIN_MODAL_ID: 'app-main-modal',
  APP_MAIN_LOADING_LAYER_ID: 'app-main-loading-layer',
  // Language
  DEFAULT_LANGUAGE: 'es', // default languages
  AVAILABLE_LANGUAGES: ['en', 'es'], // available languages
};
