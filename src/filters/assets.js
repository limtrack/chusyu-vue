import Vue from 'vue';
import constants from '@/constants';

/**
 * Add server route to remote item
 * Options availables
 *  - urlServer -> server url to commplete item URL
 */
Vue.filter('assets', (url = '', urlServer = constants.URL_SERVER) => (url ? `${urlServer}${url}` : ''));
