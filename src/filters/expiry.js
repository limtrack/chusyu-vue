import Vue from 'vue';
import moment from 'moment';

/**
 * Set expiry time (expiry time idea) pass a date (string)
 */
Vue.filter('expiry', (date = '') => {
  const today = moment();
  const currentDate = date === ''
    ? moment()
    : moment(date);
  const differenceTimeDay = currentDate.diff(today, 'days');
  const differenceTimeHour = currentDate.diff(today, 'hours');
  const differenceTimeMinute = currentDate.diff(today, 'minutes');

  if (differenceTimeMinute > 0) {
    let stringTime = '';

    if (differenceTimeMinute > 0) {
      stringTime = `${differenceTimeMinute} minutes`;
    }

    if (differenceTimeHour > 0) {
      stringTime = `${differenceTimeHour} hours`;
    }

    if (differenceTimeDay > 0) {
      stringTime = `${differenceTimeDay} days`;
    }

    return `${stringTime} to expire`;
  }
  return 'Time expired';
});
