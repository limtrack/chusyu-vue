// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
// Filters
import '@/filters/filters';
// Bootstrap
import BootstrapVue from 'bootstrap-vue';
// FontAwesomme
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faSignOutAlt,
  faBars,
  faSyncAlt,
  faUserPlus,
  faLockOpen,
  faUserCog,
  faSearch,
  faAngleLeft,
  faUsers,
  faFire,
  faLightbulb,
  faTimesCircle,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import App from './App';
import router from './router';
import store from './store';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// Bootstrap
Vue.use(BootstrapVue);
// FontAwesome
library.add([
  faSignOutAlt,
  faBars,
  faSyncAlt,
  faUserPlus,
  faLockOpen,
  faUserCog,
  faSearch,
  faAngleLeft,
  faUsers,
  faFire,
  faLightbulb,
  faTimesCircle,
]); // icons to add
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
