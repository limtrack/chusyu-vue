// Lib
import doRequest from '@/lib/request';

export default {

  /**
   * Get ideas from server
   *
   * @param {Object} params - differents options to search
   * @return {Promise} - with the server response
   */
  getIdeas(params = {}) {
    return doRequest({
      url: 'ideas',
      method: 'get',
      params,
    }, false);
  },

  /**
   * Get an idea from server
   *
   * @param {String} nameSeo - name to the product (idea) to show
   * @return {Promise} - with the server response
   */
  getIdea(nameSeo) {
    return doRequest({
      url: `ideas\\${nameSeo}`,
      method: 'get',
    }, false);
  },

  /**
   * Set an idea (create) into the server
   *
   * @param {Object} params - differents options to create the idea
   * @return {Promise} - with the server response
   */
  setIdea(params) {
    return doRequest({
      url: 'ideas/add',
      method: 'post',
      data: params,
    }, true);
  },

};
