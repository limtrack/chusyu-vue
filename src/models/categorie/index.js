// Lib
import doRequest from '@/lib/request';

export default {
  /**
   * Get categories from server
   *
   * @return {Promise} - with the server response
   */
  getCategories() {
    return doRequest({
      url: 'categories',
      method: 'get',
    }, false);
  },
};
