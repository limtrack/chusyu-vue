// Lib
import doRequest from '@/lib/request';

export default {
  /**
   * Get products from server
   *
   * @return {Promise} - with the server response
   */
  getProducts() {
    return doRequest({
      url: 'products',
      method: 'get',
    }, false);
  },
};
