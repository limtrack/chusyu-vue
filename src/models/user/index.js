// Lib
import doRequest from '@/lib/request';

export default {
  /**
   * Login user into the server
   *
   * @param {String} email - account's email
   * @param {String} password - account's password
   * @return {Promise} - with the server response
   */
  login(email, password) {
    return doRequest({
      url: 'users/authenticate',
      method: 'post',
      headers: { email, password },
    }, false);
  },

  /**
   * Save user data into the server
   *
   * @param {Object} params - account's email
   * @return {Promise} - with the server response
   */
  save(params) {
    return doRequest({
      url: 'users/save',
      method: 'post',
      data: params,
    }, true);
  },
};
