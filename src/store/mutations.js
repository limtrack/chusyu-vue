/*  eslint-disable no-param-reassign */
import mutationsName from '@/store/mutationsName';
import initStore from './initStore';

export default {
  [mutationsName.SET_REFERER_URL](state, value) {
    state.refererURL = value;
  },

  [mutationsName.SET_USER_MODEL](state, value) {
    state.user = Object.assign(state.user, value);
  },

  [mutationsName.SET_PRODUCTS_MODEL](state, value) {
    state.products = Object.assign(state.products, value);
  },

  [mutationsName.SET_CATEGORIES_MODEL](state, value) {
    state.categories = Object.assign(state.categories, value);
  },

  [mutationsName.SET_IDEAS_MODEL](state, value) {
    state.ideas = Object.assign(state.ideas, value);
  },

  [mutationsName.SET_RESET_MODEL](state) {
    Object.assign(state, initStore);
  },
};
