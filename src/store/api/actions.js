// Constants
// Contants
import constants from '@/constants';
// Models
import userModel from '@/models/user';
import productModel from '@/models/product';
import categorieModel from '@/models/categorie';
import ideaModel from '@/models/idea';
// API actions
import apiActionsName from '@/store/api/actionsName';
// Componets mutations (Vuex)
import alertMutations from '@/components/AppAlert/store/mutationsName';

const { VUEX_MODULES } = constants;
const { SET_APP_ALERT_MODEL } = alertMutations;
const {
  LOGIN_USER_API,
  UPDATE_USER_API,
  GET_PRODUCTS_API,
  GET_CATEGORIES_API,
  GET_IDEAS_API,
  GET_IDEA_API,
  SET_IDEA_API,
} = apiActionsName;

/**
 * Show error into an alert
 *
 * @param {Object} response - server response
 */
const showError = (response, commit) => {
  commit(`${VUEX_MODULES.appAlert}/${SET_APP_ALERT_MODEL}`, {
    content: response.message,
    variant: 'warning',
  });
};

export default {
  // User - Model
  [LOGIN_USER_API]({ commit }, { email, password }) {
    return userModel.login(email, password)
      .catch((response) => {
        showError.call(this, response, commit);
      });
  },

  [UPDATE_USER_API]({ commit }, params) {
    return userModel.save(params)
      .catch((response) => {
        showError.call(this, response, commit);
      });
  },

  // Product - Model
  [GET_PRODUCTS_API]({ commit }) {
    return productModel.getProducts()
      .catch((response) => {
        showError.call(this, response, commit);
      });
  },

  // Categorie - Model
  [GET_CATEGORIES_API]({ commit }) {
    return categorieModel.getCategories()
      .catch((response) => {
        showError.call(this, response, commit);
      });
  },

  // Idea - Model
  [GET_IDEAS_API]({ commit }, params = {}) {
    return ideaModel.getIdeas(params)
      .catch((response) => {
        showError.call(this, response, commit);
      });
  },

  [GET_IDEA_API]({ commit }, nameSeo) {
    return ideaModel.getIdea(nameSeo)
      .catch((response) => {
        showError.call(this, response, commit);
      });
  },

  [SET_IDEA_API]({ commit }, params) {
    return ideaModel.getIdea(params)
      .catch((response) => {
        showError.call(this, response, commit);
      });
  },

};
