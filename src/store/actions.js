import apiActions from '@/store/api/actions';
import mutationsName from '@/store/mutationsName';
import actionsName from '@/store/actionsName';

export default {
  ...apiActions,

  [actionsName.UPDATE_REFERER_URL]({ commit }, value) {
    commit(mutationsName.SET_REFERER_URL, value);
  },

  [actionsName.UPDATE_USER_MODEL]({ commit }, value) {
    commit(mutationsName.SET_USER_MODEL, value);
  },

  [actionsName.UPDATE_PRODUCTS_MODEL]({ commit }, value) {
    commit(mutationsName.SET_PRODUCTS_MODEL, value);
  },

  [actionsName.UPDATE_CATEGORIES_MODEL]({ commit }, value) {
    commit(mutationsName.SET_CATEGORIES_MODEL, value);
  },

  [actionsName.UPDATE_IDEAS_MODEL]({ commit }, value) {
    commit(mutationsName.SET_IDEAS_MODEL, value);
  },

  [actionsName.UPDATE_RESET_MODEL]({ commit }) {
    commit(mutationsName.SET_RESET_MODEL);
  },

};
