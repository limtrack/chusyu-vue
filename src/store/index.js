import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import constants from '@/constants';
import mutations from './mutations';
import actions from './actions';
import getters from './getters';
import initStore from './initStore';
// Modules
import appAlertStore from '@/components/AppAlert/store';
import appLoadingLayerStore from '@/components/AppLoadingLayer/store';
import appModalStore from '@/components/AppModal/store';

const { LOCALSTORAGE_VUEX_KEY, VUEX_MODULES } = constants;
const state = initStore;

Vue.use(Vuex);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  plugins: [createPersistedState({ key: LOCALSTORAGE_VUEX_KEY })],
  modules: {
    [VUEX_MODULES.appAlert]: {
      namespaced: true,
      ...appAlertStore,
    },
    [VUEX_MODULES.appLoadingLayer]: {
      namespaced: true,
      ...appLoadingLayerStore,
    },
    [VUEX_MODULES.appModal]: {
      namespaced: true,
      ...appModalStore,
    },
  },
});
