export default {
  /**
   * return logged user status
   * @param state
   * @returns {Boolean}
   */
  isUserLogged: (state) => Boolean(state.user) && Boolean(state.user.email),
  /**
   * return logged user data
   * @param state
   * @returns {Object}
   */
  loggedUser: (state) => state.user || {},
  /**
   * return available products data (registers)
   * @param state
   * @returns {Array}
   */
  availableProducts: (state) => state.products.registers || [],
  /**
   * return available categorios from products data (registers)
   * @param state
   * @returns {Array}
   */
  availableCategories: (state) => state.categories.registers || [],
  /**
   * return ideas data (registers)
   * @param state
   * @returns {Array}
   */
  activatedIdeas: (state) => state.ideas.registers || [],
  /**
   * return status user logged
   * @param state
   * @returns {Boolean}
   */
  refererURL: (state) => {
    const url = Object.keys(state.refererURL).length > 0 ? state.refererURL : false;
    return url;
  },
};
