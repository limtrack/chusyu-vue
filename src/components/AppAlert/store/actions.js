import mutationsName from './mutationsName';
import actionsName from './actionsName';

export default {
  /**
   * Update the state
   */
  [actionsName.UPDATE_APP_ALERT_MODEL]: ({ commit }, value) => {
    commit(mutationsName.SET_APP_ALERT_MODEL, value);
  },
  /**
   * Reset the state
   */
  [actionsName.UPDATE_RESET_APP_ALERT_MODEL]: ({ commit }) => {
    commit(mutationsName.RESET_APP_ALERT_MODEL);
  },
};
