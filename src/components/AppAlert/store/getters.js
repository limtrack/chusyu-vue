export default {
  /**
   * return class object app-alert
   * @param state
   * @returns {Object}
   */
  getDataAppAlert: (state) => state || {},
};
