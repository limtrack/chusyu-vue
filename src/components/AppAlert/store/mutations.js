/* eslint-disable no-param-reassign */
import mutationsName from './mutationsName';
import defaultStore from './defaultStore';

export default {
  /**
   * Update state app alert
   */
  [mutationsName.SET_APP_ALERT_MODEL]: (state, value) => {
    state = Object.assign(state, value);
  },
  /**
   * Reset state app alert
   */
  // eslint-disable-next-line no-unused-vars
  [mutationsName.RESET_APP_ALERT_MODEL]: (state) => {
    state = Object.assign(state, defaultStore);
  },
};
