export default {
  /**
   * return app modal attributes
   * @param state
   * @returns {Object}
   */
  getDataAppModal: (state) => state || {},
};
