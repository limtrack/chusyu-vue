/* eslint-disable no-param-reassign */
import mutationsName from './mutationsName';
import defaultStore from './defaultStore';

export default {
  [mutationsName.SET_APP_MODAL_MODEL]: (state, value) => {
    state = Object.assign(state, value);
  },

  // eslint-disable-next-line no-unused-vars
  [mutationsName.SET_APP_MODAL_RESET_MODEL]: (state) => {
    state = Object.assign(state, defaultStore);
  },
};
