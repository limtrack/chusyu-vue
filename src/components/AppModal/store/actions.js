import mutationsName from './mutationsName';
import actionsName from './actionsName';

export default {
  [actionsName.UPDATE_APP_MODAL_MODEL]: ({ commit }, value) => {
    commit(mutationsName.SET_APP_MODAL_MODEL, value);
  },

  [actionsName.UPDATE_APP_MODAL_RESET_MODEL]: ({ commit }) => {
    commit(mutationsName.SET_APP_MODAL_RESET_MODEL);
  },
};
