import mutationsName from './mutationsName';
import actionsName from './actionsName';

export default {
  /**
   * Update the state
   */
  [actionsName.UPDATE_APP_LOADING_LAYER_MODEL]: ({ commit }, value) => {
    commit(mutationsName.SET_APP_LOADING_LAYER_MODEL, value);
  },
  /**
   * Reset the state
   */
  [actionsName.UPDATE_APP_LOADING_LAYER_RESET_MODEL]: ({ commit }) => {
    commit(mutationsName.RESET_APP_LOADING_LAYER_MODEL);
  },
};
