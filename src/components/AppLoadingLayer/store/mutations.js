/* eslint-disable no-param-reassign */
import mutationsName from './mutationsName';
import defaultStore from './defaultStore';

export default {
  /**
   * Update state app loading layer
   */
  [mutationsName.SET_APP_LOADING_LAYER_MODEL]: (state, value) => {
    state = Object.assign(state, value);
  },
  /**
   * Reset state app loading layer
   */
  // eslint-disable-next-line no-unused-vars
  [mutationsName.RESET_APP_LOADING_LAYER_MODEL]: (state) => {
    state = Object.assign(state, defaultStore);
  },
};
