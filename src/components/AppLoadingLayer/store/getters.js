export default {
  /**
   * return class object app-loading-layer
   * @param state
   * @returns {Object}
   */
  getDataAppLoadingLayer: (state) => state || {},
};
