export default {
  RESET_APP_LOADING_LAYER_MODEL: 'RESET_APP_LOADING_LAYER_MODEL',
  SET_APP_LOADING_LAYER_MODEL: 'SET_APP_LOADING_LAYER_MODEL',
};
